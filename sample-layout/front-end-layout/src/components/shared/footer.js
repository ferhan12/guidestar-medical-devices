import React from "react";
import { Container } from "reactstrap";
import logo from "./logo1.jpg";

const Footer = () => {
  return (
    <footer className="py-5 bg-dark">
      <Container>
        <p className="m-0 text-center text-white">
          Copyright &copy; GuideStar 2021
        </p>
        <p12> <ui>
          {" "}
          Our Address 201-2067 Cadboro Bay Road Victoria, BC, Canada V8R 5G4 ​
          Office: 250-940-0015{" "}  </ui>
        </p12>
        ​
        <img alt="logo" className="photo" src={logo} />
      </Container>
    </footer>
  );
};

export default Footer;
