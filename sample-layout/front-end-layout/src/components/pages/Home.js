import React from "react";
import {
  Container,
  Row,
  Col,
  CardBody,
  CardTitle,
  CardText,
  CardFooter,
  Card,
} from "reactstrap";
import ReactPlayer from "react-player";

const Home = () => {
  return (
    <Container>
      <Row className="my-5">
        <Col lg="7">
          <ReactPlayer
            className="vid"
            url="https://www.youtube.com/watch?v=lqEsFZFTm3k"
          />
        </Col>

        <Col lg="5">
          <h1 className="title">Guide Star Medical Devices</h1>
          <p>
            GuideStar Medical Devices is a med-tech start-up based in Victoria,
            British Columbia. Our team is committed to delivering world-class
            solutions to real medical problems, transforming the way doctors use
            technology. Our goal is to design and manufacture ingenious medical
            devices that transform medical procedures, improve safety, and
            reduce cost, with solutions that are accessible to practitioners
            around the world
          </p>
        </Col>
      </Row>
      <Card className="text-white bg-secondary my-5 py-4 text-center">
        <CardBody>
          <CardText>
            <p className="cd">
              Committed to delivering world-class solutions to current medical
              problems
            </p>
          </CardText>
        </CardBody>
      </Card>
      <Row>
        <Col md="4" className="mb-5">
          <Card>
            <CardBody>
              <CardTitle>
                <h11>Transforming the way doctors use technology</h11>
              </CardTitle>
              <CardBody> <p>
                  <li>Some text</li></p>
                  </CardBody>
            </CardBody>
            <CardFooter>
    
            </CardFooter>
          </Card>
        </Col>
        <Col md="4" className="mb-5">
          <Card>
            <CardBody>
              <CardTitle>
                <h11>INFO on device</h11>
              </CardTitle>
              <CardBody> <p>
                  <li>Some text</li> </p>
                  
                  </CardBody>
            </CardBody>
            <CardFooter>
              
            </CardFooter>
          </Card>
        </Col>
        <Col md="4" className="mb-5">
          <Card>
            <CardBody>
              <CardTitle>
                <h11>Facts/ Stats on device</h11>
              </CardTitle>        
              <CardBody> <p>
                  
                  <li>Some text</li> </p>
                  </CardBody>
            </CardBody>
            <CardFooter>
             
            </CardFooter>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Home;
