import React from 'react'
import { Container, Row, Col,} from 'reactstrap'
import epi from "./epi.jpg";

const device = () => {
    return (
        <Container>
        <Row className="my-5">
            <Col lg="7">
            <img alt="epidural" className="epi" src={epi} />
            </Col>
            <Col lg="5">
                <h1 className="font-weight-light">Device</h1>
                <p>All device related info can go here. Alongside blogs/media info </p>
                
            </Col>
        </Row>
    </Container>
    )
}

export default device
