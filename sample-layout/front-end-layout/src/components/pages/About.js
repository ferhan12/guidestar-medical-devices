import React from "react";
import { Container, Row, Col,} from "reactstrap";
import dr1 from "./dr1.jpg";
import ry from "./ry.jpg";
import bc from "./bc.jpg";
import james from "./James.jpg";
import drj from "./drj.jpg";

const About = () => {
  return (
    <Container> 
      <h9> Leadership Team</h9>
      <Row className="my-5">
        <Col lg="7">
          <img alt="Mike Dolphin" className="dr" src={dr1} />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light"> Mike Dolphin - CEO</h1>
          <p>
            {" "}
            Mike Dolphin earned a Masters degree and PhD from Stanford
            University in Aeronautical and Astronautical Engineering. He
            completed his Honours Bachelor of Science degree at the University
            of Western Ontario in Astronomy and Geophysics. Dr. Dolphin has a
            diverse background including work in precision engineering ands
            manufacturing, rocket design, satellite component engineering,
            geophysics and computer software production. He has managed
            engineering teams on spaceflight projects and teams for software
            development. For the past six years, he has led the design and
            manufacture of medical devices. GuideStar Medical Devices is his
            first startup as CEO{" "}
          </p>
        </Col>
      </Row>

      <Row className="my-5">
        <Col lg="7">
          <img alt="Ryan Ruedebusch " className="dr" src={ry} />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">
            {" "}
            Ryan Ruedebusch - VP Business Development
          </h1>
          <p>
            {" "}
            Ryan Ruedebusch has extensive leadership and operational experience
            from several roles, primarily with scientific companies. Most
            recently, Ryan spent 17 years with VWR where he led a large and
            interdisciplinary team towards profitability and was responsible for
            the Pricing Strategy & Operations functions across the company's
            multi-billion dollar business. Ryan has also held a variety of sales
            and leadership roles at a large global distributor of scientific
            supplies, in addition to previous experience running a family owned
            business. Ryan holds a BS in Environmental Science from St. Cloud
            State University. When not working, Ryan can be found chasing after
            two children, running, or golfing.{" "}
          </p>
        </Col>
      </Row>

      <Row className="my-5">
        <Col lg="7">
          <img alt=" Brianna Carrels " className="dr" src={bc} />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">
            {" "}
            Brianna Carrels - Engineering Project Manager
          </h1>
          <p>
            {" "}
            Brianna Carrels has a Bachelor of Engineering from the University of
            Victoria in Biomedical Engineering. She has a background working in
            hospitals and the medical device and pharmaceuticals industries.
            Brianna has led multiple engineering teams, and has experience in
            project management, product development and stakeholder management.
            She is the University of Victoria 2017-18 Co-op Student of the Year,
            recipient of the BC Tech Innovation Award and IEEE Gold Medal in
            Biomedical Engineering. She was also a member of the UVic women's
            varsity golf team, and still loves playing golf.{" "}
          </p>
        </Col>
      </Row>

      <h9> Board of Directors</h9>
      <Row className="my-5">
        <Col lg="7">
          <img alt=" James Helliwell " className="dr" src={james} />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">
            {" "}
            James Helliwell - MD FRCPC, Chairman
          </h1>
          <p>
            {" "}
            Eupraxia Pharmaceuticals founder holds a medical degree from the
            University of British Columbia (UBC), with specialization in
            anesthesiology. He also received sub-specialty training in cardiac
            anesthesiology and transplantation. Prior to founding Eupraxia, he
            held a clinical practice at a quaternary academic cardiac center in
            St. Paul's Hospital, Vancouver. He also served as Clinical Assistant
            Professor at the University of British Columbia in the Department of
            Anesthesiology, Pharmacology and Therapeutics.{" "}
          </p>
        </Col>
      </Row>
      <Row className="my-5">
        <Col lg="7">
          <img alt="Mike Dolphin" className="dr" src={dr1} />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">
            {" "}
            Dr. Michael Dolphin - PhD, Director
          </h1>
          <p>
            {" "}
            Dr. Dolphin completed his Honours Bachelor of Science at the
            University of Western Ontario in Astronomy and Geophysics. He
            received his Masters and Doctorate from Stanford University in
            Aeronautical and Astronautical Engineering. Dr. Dolphin has a
            diverse background including work in precision engineering and
            manufacture, rocket design, satellite component engineering,
            geophysics and computer software production. He has managed
            engineering teams on spaceflight projects and has experience
            consulting with companies preparing their SR&ED claims. Mike has
            also served for over five years as the Chief Technology Officer of
            Accuro Technologies, a medical device startup.{" "}
          </p>
        </Col>
      </Row>
      <Row className="my-5">
        <Col lg="7">
          <img alt="Dr Jack Pacey" className="dr" src={drj} />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">
            {" "}
            Dr. Jack Pacey - MD, Director
          </h1>
          <p>
            {" "}
            Dr. Jack Pacey has been practicing medicine for over 40 years. In
            addition to being a surgeon at Burnaby Hospital for over 30 years,
            he is also the founder of Saturn Biomedical Systems (acquired by
            Verathon Medical Canada in 2006) and PaceyMedTech.{" "}
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default About;
